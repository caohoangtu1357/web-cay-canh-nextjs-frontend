import Link from "next/link";
import Head from "next/head";
import styles from "./style.module.css";

export default function HelloWorld() {
    return(
        <div>
            <Head>
                <title>Cây cảnh</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <h1 className={styles.head}>Home page</h1>
            <h2>
                <Link href="/order"><a>order</a></Link>
            </h2>
        
        </div>

    );
}